from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Notes(models.Model):
    user=models.ForeignKey(User, on_delete=models.CASCADE)
    title=models.CharField(max_length=200)
    description=models.TextField()
    
    def __str__(self):
        return self.title

    class Meta:
        verbose_name="Notes"
        verbose_name_plural="Notes"



class Homework(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    subject=models.CharField(max_length=50)
    title=models.CharField(max_length=100)
    description=models.TextField()
    due=models.DateTimeField()
    is_finished=models.BooleanField(default=False)
 
    def __str__(self):
        return self.title


class Todo(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    is_finished = models.BooleanField(default=False)

    def __str__(self):
        return self.title


class Bookmark(models.Model):
    url =models.CharField(max_length=1000)
    title=models.CharField(max_length=200)
    category=models.CharField(max_length=20)
    user = models.ForeignKey(User,on_delete=models.CASCADE)



class Marks(models.Model):
        user = models.ForeignKey(User,on_delete=models.CASCADE)
        subject=models.CharField(max_length=20)
        marks=models.IntegerField()

        @property
        def get_grade(self):
            if self.marks >90<=100:
                return 'A'
            elif self.marks >80<=90:
                return 'B'
            elif self.marks >70<=80:
                return 'C'
            elif self.marks >60<=70:
                return 'D'
            elif self.marks >40<=60:
                return 'E'
            elif self.marks <40:
                return 'F'
            else:
                return 'Invalid Input'

